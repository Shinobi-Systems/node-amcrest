const apiHost = process.argv[2]
const username = process.argv[3]
const password = process.argv[4]

if(!apiHost || !username || !password){
    console.log(`Test Failed. Missing Parameters.`)
    console.log(`Usage : node ./test.js API_HOST CAMERA_USERNAME CAMERA_PASSWORD`)
    console.log(`Example : node ./test.js "http://10.0.0.100" myusername mypassword`)
    return;
}

const amcrestAPI = require('./index.js')

const {
    apiRequest,
    getCurrentTime,
    getGeneralConfiguration,
    getEncodingConfiguration,
    getEncodingConfigurationCapabilities,
} = amcrestAPI(apiHost,username,password);

async function runTest(){
    const getGeneralConfigurationResponse = await getGeneralConfiguration();
    console.log(`getGeneralConfigurationResponse`,getGeneralConfigurationResponse);
    const getCurrentTimeResponse = await getCurrentTime();
    console.log(`getCurrentTimeResponse`,getCurrentTimeResponse)
    const getEncodingConfigurationCapabilitiesResponse = await getEncodingConfigurationCapabilities();
    console.log(`getEncodingConfigurationCapabilitiesResponse`,JSON.stringify(getEncodingConfigurationCapabilitiesResponse,null,3))
    const getEncodingConfigurationResponse = await getEncodingConfiguration();
    console.log(`getEncodingConfigurationResponse`,JSON.stringify(getEncodingConfigurationResponse,null,3))
    return 'Done test!'
}

runTest().then((data) => {
    console.log(data)
    console.log(`Amcrest HTTP API by Moinul (Moe) Alam, Shinobi Systems`)
})
