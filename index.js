const fetch = require('node-fetch')
const DigestFetch = require('digest-fetch')
module.exports = (apiHost,username,password,debug) => {
    const client = new DigestFetch(username, password)
    console.log(`Loading Amcrest HTTP API by Moinul (Moe) Alam, Shinobi Systems`)
    function responseParser(string){
        const response = {}
        const lines = string.trim().split('\n')
        lines.forEach((line) => {
            const lineParts = line.trim().split('=')
            const rawKey = lineParts[0].startsWith('table.') ? lineParts[0].replace('table.','') : lineParts[0]
            const value = lineParts[1]
            // if(rawKey.indexOf('.') === -1){
                response[rawKey] = value//.indexOf(',') > -1 ? value.split(',') : value
            // }else{
            //     const keyParts = rawKey.split('.')
            //     if(!response[keyParts[0]])response[keyParts[0]] = {}
            //     let currentLayer = response[keyParts[0]]
            //     keyParts.forEach((keyLayer,n) => {
            //         switch(n){
            //             case 0:
            //                 if(!response[keyLayer])response[keyLayer] = {}
            //             break;
            //             case keyParts.length - 1:
            //                 currentLayer[keyLayer] = value.indexOf(',') > -1 ? value.split(',') : value
            //             break;
            //             default:
            //                 if(!currentLayer[keyLayer])currentLayer[keyLayer] = {}
            //             break;
            //         }
            //         currentLayer = currentLayer[keyLayer] || currentLayer
            //     })
            // }
        })
        return response
    }
    function apiRequest(apiPoint,options){
        return new Promise((resolve,reject) => {
            options = options || {};
            const host = apiHost || `http://172.16.101.163`
            const fetchUrl = `${host}/${options.innerUri || 'cgi-bin/'}${apiPoint}.cgi`
            const queryData = Object.assign({action: ''},options.body || {},{
                action: options.action
            });
            const response = {
                ok: false
            }
            if(!queryData.action){
                response.err = 'No Action Provided'
                resolve(queryData)
            }
            let queryString = []
            for(const queryKey in queryData){
                const queryValue = queryData[queryKey]
                queryString.push(`${queryKey}=${queryValue}`)
            }
            const finalFetch = `${fetchUrl}${queryString ? `?${queryString.join('&')}` : ''}`
            client.fetch(finalFetch)
            .then(response => {
                if(debug){
                    console.log(response)
                    console.log(response.headers)
                }
                return response.text()
            })
            .then((data) => {
                if(debug)console.log(data);
                response.ok = true
                response.response = data
                response.data = responseParser(data)
                response.from = finalFetch
                resolve(response)
            })
            .catch(err => {
                response.err = err
                resolve(response)
            })
        })
    }
    function getEncodingConfiguration(){
        return apiRequest(`configManager`,{
            action: 'getConfig',
            body: {
                name: 'Encode'
            }
        })
    }
    function setConfiguration(options){
        return apiRequest(`configManager`,{
            action: 'setConfig',
            body: options
        })
    }
    function getEncodingConfigurationCapabilities(){
        return apiRequest(`encode`,{
            action: 'getConfigCaps',
            body: {
                channel: 1
            }
        })
    }
    function getCurrentTime(){
        return apiRequest(`global`,{
            action: 'getCurrentTime',
        })
    }
    function getGeneralConfiguration(){
        return apiRequest(`configManager`,{
            action: 'getConfig',
            body: {
                name: 'General',
            }
        })
    }
    return {
        apiRequest,
        getCurrentTime,
        setConfiguration,
        getGeneralConfiguration,
        getEncodingConfiguration,
        getEncodingConfigurationCapabilities,
    }
}
