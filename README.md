# Node Amcrest

Unofficial Amcrest API Wrapper for Node.js by Shinobi Systems (https://shinobi.video)

All HTTP API Requests seen here https://s3.amazonaws.com/amcrest-files/Amcrest+HTTP+API+3.2017.pdf should work with this wrapper.

Initialize Wrapper

```
const amcrestAPI = require('node-amcrest')

const {
    apiRequest
} = amcrestAPI(apiHost,username,password);
```

Simple API Calls with `apiRequest`. First Parameter is the target, Second are the options.

```
// http://CAMERA_IP/cgi-bin/global.cgi?action=getCurrentTime
apiRequest(`global`,{
    action: 'getCurrentTime',
}).then((data) => {
    console.log(data)
})
```

In this one you can see `body` this is for adding additional parameters to the call. This one needed `name: 'General'`.

```
apiRequest(`configManager`,{
    action: 'getConfig',
    body: {
        name: 'General',
    }
}).then((data) => {
    console.log(data)
})
```

Contents of Test.js

```
const apiHost = process.argv[2]
const username = process.argv[3]
const password = process.argv[4]

if(!apiHost || !username || !password){
    console.log(`Test Failed. Missing Parameters.`)
    console.log(`Usage : node ./test.js API_HOST CAMERA_USERNAME CAMERA_PASSWORD`)
    console.log(`Example : node ./test.js "http://10.0.0.100" myusername mypassword`)
    return;
}

const amcrestAPI = require('./index.js')

const {
    apiRequest,
    getCurrentTime,
    getGeneralConfiguration,
    getEncodingConfiguration,
    getEncodingConfigurationCapabilities,
} = amcrestAPI(apiHost,username,password);

async function runTest(){
    const getGeneralConfigurationResponse = await getGeneralConfiguration();
    console.log(`getGeneralConfigurationResponse`,getGeneralConfigurationResponse);
    const getCurrentTimeResponse = await getCurrentTime();
    console.log(`getCurrentTimeResponse`,getCurrentTimeResponse)
    const getEncodingConfigurationCapabilitiesResponse = await getEncodingConfigurationCapabilities();
    console.log(`getEncodingConfigurationCapabilitiesResponse`,JSON.stringify(getEncodingConfigurationCapabilitiesResponse,null,3))
    const getEncodingConfigurationResponse = await getEncodingConfiguration();
    console.log(`getEncodingConfigurationResponse`,JSON.stringify(getEncodingConfigurationResponse,null,3))
    return 'Done test!'
}

runTest().then((data) => {
    console.log(data)
    console.log(`Amcrest HTTP API by Moinul (Moe) Alam, Shinobi Systems`)
})
```
